---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-exporter
data:
  collector.yaml: |
    extensions:
      health_check:
        endpoint: "0.0.0.0:13133"
      bearertokenauth/server:
        scheme: "Bearer"
        filename: /token/server
      bearertokenauth/client:
        scheme: "Bearer"
        filename: /token/client
    receivers:
      otlp:
        protocols:
          grpc:
            endpoint: ${POD_IP}:4317
      otlp/mtls:
        protocols:
          grpc:
            endpoint: ${POD_IP}:4318
            tls:
             client_ca_file: /egress-tls/ca.crt
             cert_file: /egress-tls/tls.crt
             key_file: /egress-tls/tls.key
      otlp/auth:
        protocols:
          grpc:
            endpoint: ${POD_IP}:4319
            auth:
              authenticator: bearertokenauth/server
    processors:
      memory_limiter:
        check_interval: 1s
        limit_percentage: 80
        spike_limit_percentage: 25
      batch:
        timeout: 5s
        send_batch_size: 50000
    exporters:
    {{- if .Values.exporter.clickhouse.enabled }}
      clickhouse:
        endpoint: tcp://clickhouse.clickhouse.svc.cluster.local:9440?secure=true&skip_verify=true&dial_timeout=10s
        compress: lz4
        database: otel
        cluster_name: default
        table_engine:
          name: ReplicatedMergeTree
        username: otel-collector-exporter
        password: exporter
        ttl: 360h
        timeout: 5s
        retry_on_failure:
          enabled: true
          initial_interval: 5s
          max_interval: 30s
          max_elapsed_time: 300s
      {{- range .Values.exporter.clickhouse.tenants }}
      {{ printf "clickhouse/%s:" . }}
        endpoint: tcp://clickhouse.clickhouse.svc.cluster.local:9440?secure=true&skip_verify=true&dial_timeout=10s
        compress: lz4
        database: {{ snakecase (printf "otel_%s" .) | quote }}
        cluster_name: default
        table_engine:
          name: ReplicatedMergeTree
        username: otel-collector-exporter
        password: exporter
        ttl: 360h
        timeout: 5s
        retry_on_failure:
          enabled: true
          initial_interval: 5s
          max_interval: 30s
          max_elapsed_time: 300s
      {{- end }}
    {{- end }}
    {{- if .Values.exporter.otlp.enabled }}
      otlp:
        endpoint: {{ required "You need to define an exporter.otlp.endpoint !" .Values.exporter.otlp.endpoint }}
        tls:
          insecure: false
        headers:
          x-tenant: {{ required "You need to define an exporter.otlp.tenant !" .Values.exporter.otlp.tenant | quote }}
        auth:
          authenticator: bearertokenauth/client
    {{- end }}
    {{- if gt (len .Values.exporter.clickhouse.tenants) 0 }}
    connectors:
      routing/logs:
        default_pipelines: [logs/clickhouse]
        table:
          {{- range .Values.exporter.clickhouse.tenants }}
          - context: request
            condition: request["x-tenant"] == {{ . | quote }}
            pipelines: [{{ printf "logs/clickhouse-%s" . | quote }}]
          {{- end }}
      routing/metrics:
        default_pipelines: [metrics/clickhouse]
        table:
          {{- range .Values.exporter.clickhouse.tenants }}
          - context: request
            condition: request["x-tenant"] == {{ . | quote }}
            pipelines: [{{ printf "metrics/clickhouse-%s" . | quote }}]
          {{- end }}
      routing/traces:
        default_pipelines: [traces/clickhouse]
        table:
          {{- range .Values.exporter.clickhouse.tenants }}
          - context: request
            condition: request["x-tenant"] == {{ . | quote }}
            pipelines: [{{ printf "traces/clickhouse-%s" . | quote }}]
          {{- end }}
    {{- end }}
    service:
      telemetry:
        logs:
          level: "warn"
      pipelines:
    {{- if .Values.exporter.clickhouse.enabled }}
    {{- if gt (len .Values.exporter.clickhouse.tenants) 0 }}
        logs/in:
          receivers: [otlp, otlp/mtls, otlp/auth]
          exporters: [routing/logs]
        metrics/in:
          receivers: [otlp, otlp/mtls, otlp/auth]
          exporters: [routing/metrics]
        traces/in:
          receivers: [otlp, otlp/mtls, otlp/auth]
          exporters: [routing/traces]
        logs/clickhouse:
          receivers: [routing/logs]
          processors: [memory_limiter, batch]
          exporters: [clickhouse]
        metrics/clickhouse:
          receivers: [routing/metrics]
          processors: [memory_limiter, batch]
          exporters: [clickhouse]
        traces/clickhouse:
          receivers: [routing/traces]
          processors: [memory_limiter, batch]
          exporters: [clickhouse]
        {{- range .Values.exporter.clickhouse.tenants }}
        {{ printf "logs/clickhouse-%s:" . }}
          receivers: [routing/logs]
          processors: [memory_limiter, batch]
          exporters: [{{ printf "clickhouse/%s" . | quote }}]
        {{ printf "metrics/clickhouse-%s:" . }}
          receivers: [routing/metrics]
          processors: [memory_limiter, batch]
          exporters: [{{ printf "clickhouse/%s" . | quote }}]
        {{ printf "traces/clickhouse-%s:" . }}
          receivers: [routing/traces]
          processors: [memory_limiter, batch]
          exporters: [{{ printf "clickhouse/%s" . | quote }}]
        {{- end }}
    {{- else }}
        logs/clickhouse:
          receivers: [otlp, otlp/mtls, otlp/auth]
          processors: [memory_limiter, batch]
          exporters: [clickhouse]
        metrics/clickhouse:
          receivers: [otlp, otlp/mtls, otlp/auth]
          processors: [memory_limiter, batch]
          exporters: [clickhouse]
        traces/clickhouse:
          receivers: [otlp, otlp/mtls, otlp/auth]
          processors: [memory_limiter, batch]
          exporters: [clickhouse]
    {{- end }}
    {{- end }}
    {{- if .Values.exporter.otlp.enabled }}
        logs/otlp:
          receivers: [otlp, otlp/mtls, otlp/auth]
          processors: [memory_limiter, batch]
          exporters: [otlp]
        metrics/clickhouse:
          receivers: [otlp, otlp/mtls, otlp/auth]
          processors: [memory_limiter, batch]
          exporters: [otlp]
        traces/clickhouse:
          receivers: [otlp, otlp/mtls, otlp/auth]
          processors: [memory_limiter, batch]
          exporters: [otlp]
    {{- end }}
      extensions: [health_check, bearertokenauth/server, bearertokenauth/client]
